<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;
use App\Activity;
use App\Affliction;
use App\Http\Resources\Activity as ActivityResource;
use App\Http\Resources\Affliction as AfflictionResource;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/activities', function () {
    return new ActivityResource(Activity::with('note')->get());
});

Route::get('/afflictions', function () {
    return new AfflictionResource(Affliction::with('note')->get());
});

Route::post('/add_activity', function (Request $request) {
    return Activity::create($request->all());
});
