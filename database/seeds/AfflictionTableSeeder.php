<?php

use Illuminate\Database\Seeder;

class AfflictionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       factory(App\Affliction::class, 5)->create();
    }
}
