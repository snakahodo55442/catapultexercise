<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Note;
use Faker\Generator as Faker;

$factory->define(Note::class, function (Faker $faker) {

    $modelType = ['App\Activity', 'App\Affliction'];

    return [
        'name' => $faker->name,
        'parent_id' => rand(0, 5),
        'parent_type' => $modelType[rand(0, 1)],
        'comment' => $faker->text,
    ];
});
