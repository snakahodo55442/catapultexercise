<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Affliction;
use Faker\Generator as Faker;

$factory->define(Affliction::class, function (Faker $faker) {
    
    $name = $faker->name;

    return [
        'title' => 'Injury of ' . $name,
        'name' => $name,
        'activity_id' => rand(1, 5),
        'description' => $faker->text,
    ];
});
