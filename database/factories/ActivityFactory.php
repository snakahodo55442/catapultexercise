<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Activity;
use Faker\Generator as Faker;

$factory->define(Activity::class, function (Faker $faker) {
    
    $name = $faker->name;

    return [
        'title' => 'Activity of ' . $name,
        'name' => $name,
        'description' => $faker->text,
    ];
});
