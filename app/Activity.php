<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{

    protected $fillable = ['name', 'title', 'description'];

    public function affliction() {
        return $this->hasMany('App\Affliction');
    }

    public function note() {
        return $this->morphMany('App\Note', 'parent');
    }
}
