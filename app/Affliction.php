<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Affliction extends Model
{
    //

    public function note() {
        return $this->morphMany('App\Note', 'parent');
    }

}
